package cn.com.tranzvision.canal;

/**
 * canal入口类
 * @author zwl
 * @date : 2021/7/19 15:42
 */
public class Entrance {
    public static void main(String[] args) {
        CanalClient canalClient = new CanalClient();
        canalClient.start();
    }
}
