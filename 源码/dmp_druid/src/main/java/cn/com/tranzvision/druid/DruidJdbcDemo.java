package cn.com.tranzvision.druid;

import java.sql.*;
import java.util.Properties;

/**
 * @author zwl
 * @date : 2021/8/3 16:40
 */
public class DruidJdbcDemo {
    public static void main(String[] args) throws Exception {
        //加载Druid JDBC驱动
        Class.forName("org.apache.calcite.avatica.remote.Driver");
        //获取Druid JDBC连接
        Connection connection = DriverManager.getConnection("jdbc:avatica:remote:url=http://k8s03:8888/druid/v2/sql/avatica/", new Properties());

        //构建SQL语句
        String sql = "SELECT * FROM \"metrics-kafka\"";

        //构建Statement，执行SQL获取结果集
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        while (resultSet.next()){
            String url = resultSet.getString("url");
            System.out.println(url);
        }

        resultSet.close();
        statement.close();
        connection.close();
    }
}
