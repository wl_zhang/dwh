package cn.com.tranzvision.phoenix;

import java.sql.*;

/**
 * @author zwl
 * @date : 2021/7/30 11:07
 */
public class PhoenixDemo {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Class.forName("org.apache.phoenix.jdbc.PhoenixDriver");

        Connection connection = DriverManager.getConnection("jdbc:phoenix:k8s01:2181", "", "");
        Statement statement = connection.createStatement();

        ResultSet resultSet = statement.executeQuery("select * from \"dwd_order_detail\" limit 10 ");

        while(resultSet.next()){
            String rowid = resultSet.getString("rowid");
            String ogId = resultSet.getString("ogId");
            String orderId = resultSet.getString("orderId");
            String goodsId = resultSet.getString("goodsId");
            String goodsNum = resultSet.getString("goodsNum");
            String goodsPrice = resultSet.getString("goodsPrice");
            String goodsName = resultSet.getString("goodsName");
            String shopId = resultSet.getString("shopId");
            String goodsThirdCatId = resultSet.getString("goodsThirdCatId");
            String goodsThirdCatName = resultSet.getString("goodsThirdCatName");
            String goodsSecondCatId = resultSet.getString("goodsSecondCatId");
            String goodsSecondCatName = resultSet.getString("goodsSecondCatName");
            String goodsFirstCatId = resultSet.getString("goodsFirstCatId");
            String goodsFirstCatName = resultSet.getString("goodsFirstCatName");
            String areaId = resultSet.getString("areaId");
            String shopName = resultSet.getString("shopName");
            String shopCompany = resultSet.getString("shopCompany");
            String cityId = resultSet.getString("cityId");
            String cityName = resultSet.getString("cityName");
            String regionId = resultSet.getString("regionId");
            String regionName = resultSet.getString("regionName");

            System.out.println(rowid);
            System.out.println(ogId);
            System.out.println(orderId);
            System.out.println(goodsId);
            System.out.println(goodsNum);
            System.out.println(goodsPrice);
            System.out.println(goodsName);
            System.out.println(shopId);
            System.out.println(goodsThirdCatId);
            System.out.println(goodsThirdCatName);
            System.out.println(goodsSecondCatId);
            System.out.println(goodsSecondCatName);
            System.out.println(goodsFirstCatId);
            System.out.println(goodsFirstCatName);
            System.out.println(areaId);
            System.out.println(shopName);
            System.out.println(shopCompany);
            System.out.println(cityId);
            System.out.println(cityName);
            System.out.println(regionId);
            System.out.println(regionName);
        }
        resultSet.close();
        statement.close();
        connection.close();
    }
}
